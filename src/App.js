import Navbar from "./components/Navbar";
import Routes from "./routes";
import GlobalStyle from "./styles/global";

function App() {
  return (
    <div className="App">
      <GlobalStyle />
      <Navbar />
      <Routes />
    </div>
  );
}

export default App;
