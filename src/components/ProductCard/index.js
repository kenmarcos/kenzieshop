import {
  Grid,
  Typography,
  Button,
  CardActions,
  CardContent,
  CardMedia,
  CardActionArea,
  Card,
} from "@material-ui/core";
import { useDispatch } from "react-redux";
import { addToCartThunk } from "../../store/modules/cart/thunks";
import { makeStyles } from "@material-ui/core/styles";

const useStyle = makeStyles({
  root: {
    height: "100%",
  },

  image: {
    height: "16rem",
  },

  button: {
    justifyContent: "center",
    padding: "25px",
  },
});

const ProductCard = ({ product }) => {
  const classes = useStyle();

  const dispatch = useDispatch();

  return (
    <Grid item xs={12} sm={6} lg={3}>
      <Card className={classes.root}>
        <CardActionArea>
          <CardMedia
            className={classes.image}
            component="img"
            image={product.image}
            title={product.name}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {product.name}
            </Typography>
            <Typography variant="h6" color="textSecondary" component="p">
              Preço: R${product.price}
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions className={classes.button}>
          <Button
            variant="contained"
            color="primary"
            onClick={() => dispatch(addToCartThunk(product))}
          >
            Adicionar ao carrinho
          </Button>
        </CardActions>
      </Card>
    </Grid>
  );
};

export default ProductCard;
