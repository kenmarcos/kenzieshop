import { makeStyles } from "@material-ui/core/styles";
import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  Link,
  Badge,
} from "@material-ui/core";
import ShoppingCartOutlinedIcon from "@material-ui/icons/ShoppingCartOutlined";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";

const useStyles = makeStyles({
  title: {
    flexGrow: 1,
    fontFamily: "Otomanopee One, sans-serif",
    fontSize: "2.5rem",
  },
});

const Navbar = () => {
  const classes = useStyles();

  const history = useHistory();

  const cart = useSelector((store) => store.cart);

  return (
    <AppBar position="sticky">
      <Toolbar>
        <Typography className={classes.title} variant="h2">
          <Link href="/" color="inherit" underline="none">
            Kenzie Shop
          </Link>
        </Typography>
        <IconButton color="inherit" onClick={() => history.push("/cart")}>
          <Badge badgeContent={cart.length} color="error">
            <ShoppingCartOutlinedIcon />
          </Badge>
        </IconButton>
      </Toolbar>
    </AppBar>
  );
};

export default Navbar;
