import {
  Grid,
  Typography,
  Card,
  CardMedia,
  CardContent,
  CardActions,
  Button,
  Hidden,
} from "@material-ui/core";
import { useDispatch } from "react-redux";
import { removeFromCartThunk } from "../../store/modules/cart/thunks";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import { makeStyles } from "@material-ui/core/styles";

const useStyle = makeStyles((theme) => ({
  root: {
    display: "flex",
    padding: theme.spacing(2),
  },

  img: {
    width: "15%",
  },

  content: {
    flexGrow: 1,
  },
}));

const ProductInCart = ({ product, index }) => {
  const classes = useStyle();

  const dispatch = useDispatch();

  return (
    <Grid item xs={12}>
      <Card className={classes.root}>
        <Hidden xsDown>
          <CardMedia
            className={classes.img}
            component="img"
            image={product.image}
            hidden
          />
        </Hidden>
        <CardContent className={classes.content}>
          <Typography variant="h5" component="h2">
            {product.name}
          </Typography>
          <Typography variant="h6" color="textSecondary" component="p">
            Preço: R${product.price}
          </Typography>
        </CardContent>
        <CardActions>
          <Button
            endIcon={<DeleteForeverIcon />}
            variant="contained"
            color="primary"
            onClick={() => dispatch(removeFromCartThunk(index))}
          >
            Remover
          </Button>
        </CardActions>
      </Card>
    </Grid>
  );
};

export default ProductInCart;
