import { Switch, Route } from "react-router-dom";
import Cart from "../pages/Cart";
import Products from "../pages/Products";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Products />
      </Route>
      <Route>
        <Cart path="/cart" />
      </Route>
    </Switch>
  );
};

export default Routes;
