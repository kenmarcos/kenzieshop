import { useSelector } from "react-redux";
import { Container, Grid, Box, Typography } from "@material-ui/core";
import ProductCard from "../../components/ProductCard";

const Products = () => {
  const products = useSelector((store) => store.products);

  return (
    <Container>
      <Box p={3}>
        <Typography
          variant="h3"
          component="h1"
          align="center"
          color="secondary"
        >
          Produtos
        </Typography>
      </Box>
      <Grid container spacing={2}>
        {products.map((product) => (
          <ProductCard key={product.id} product={product} />
        ))}
      </Grid>
    </Container>
  );
};

export default Products;
