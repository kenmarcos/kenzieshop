import { ADD_TO_CART, REMOVE_FROM_CART } from "./actionTypes";
const defaultState = JSON.parse(localStorage.getItem("@kenzieshop:cart")) || [];

const cartReducer = (state = defaultState, action) => {
  switch (action.type) {
    case ADD_TO_CART:
      const { product } = action;
      return [...state, product];

    case REMOVE_FROM_CART:
      const { list } = action;
      return list;

    default:
      return state;
  }
};

export default cartReducer;
