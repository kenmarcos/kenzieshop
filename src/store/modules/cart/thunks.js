import { addToCart, removeFromCart } from "./actions";

export const addToCartThunk = (product) => {
  return (dispatch) => {
    const list = JSON.parse(localStorage.getItem("@kenzieshop:cart")) || [];
    list.push(product);
    localStorage.setItem("@kenzieshop:cart", JSON.stringify(list));

    dispatch(addToCart(product));
  };
};

export const removeFromCartThunk = (index) => {
  return (dispatch, getStore) => {
    const { cart } = getStore();

    const list = cart.filter((_, idx) => idx !== index);

    localStorage.setItem("@kenzieshop:cart", JSON.stringify(list));

    dispatch(removeFromCart(list));
  };
};
