const products = [
  {
    id: 1,
    name: "Teclado",
    price: 200.0,
    image:
      "https://images.tcdn.com.br/img/img_prod/740836/teclado_gamer_rgb_k_mex_4181_1_5849b3344144db8385c4e09639e0a72f.jpg",
  },
  {
    id: 2,
    name: "Tablet",
    price: 2000.0,
    image:
      "https://cdn.pixabay.com/photo/2013/09/22/09/15/tablet-184888_960_720.jpg",
  },
  {
    id: 3,
    name: "Fone de ouvido",
    price: 200.0,
    image:
      "https://cdn.pixabay.com/photo/2018/09/17/14/27/headphones-3683983_960_720.jpg",
  },
  {
    id: 4,
    name: "Notebook",
    price: 3000.0,
    image:
      "https://pixnio.com/free-images/2018/06/28/2018-06-28-08-13-43-900x900.jpg",
  },
  {
    id: 5,
    name: "Mouse",
    price: 200.0,
    image:
      "https://cdn.pixabay.com/photo/2012/03/01/01/33/mouse-20223_960_720.jpg",
  },
  {
    id: 6,
    name: "Caneca",
    price: 40.0,
    image:
      "https://http2.mlstatic.com/D_NQ_NP_666386-MLB42320866516_062020-O.jpg",
  },
  {
    id: 7,
    name: "Camiseta",
    price: 60.0,
    image:
      "https://ae01.alicdn.com/kf/H9220f62b52c54c1ca46759553b9cfa88L/Camiseta-quadro-de-javascript-logotipo-oficial-grande-softstyle-react-js.jpg_q50.jpg",
  },
  {
    id: 8,
    name: "Pato de borracha",
    price: 20.0,
    image:
      "https://cdn.pixabay.com/photo/2019/04/14/20/05/duck-meet-4127713_960_720.jpg",
  },
];

export default products;
