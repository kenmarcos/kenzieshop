# Kenzie Shop

Link do site: https://kenzieshop-seven-theta.vercel.app/

## Sobre o Projeto

Esse projeto é parte integrante das atividades de ensino realizadas na Kenzie Academy Brasil. O objetivo era criar uma aplicação um pouco mais complexa, utilizando conceitos mais robustos aprendidos durante o curso. Foi proposto então, a criação de um carrinho de e-commerce.

## Conhecimentos aplicados

React JS | Redux | Material UI
